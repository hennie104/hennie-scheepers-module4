// ignore_for_file: prefer_const_constructors
import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:trash_away/screens/dashboard.dart';
import 'package:trash_away/screens/mainpage.dart';
import 'package:trash_away/utils/utils.dart';

class Login extends StatefulWidget {
  final VoidCallback SignUpNow;

  const Login({Key? key, required this.SignUpNow}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  // text controllers
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final GlobalKey<NavigatorState> myNavigatorKey = GlobalKey<NavigatorState>();

  Future logIn() async {
    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: _emailController.text.trim(),
          password: _passwordController.text.trim());
      log('Success!');
      runApp(MainPage());
    } on FirebaseAuthException catch (e) {
      print(e.toString());

      Utils.showSnackBar(e.toString());
    }
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(
                      'https://images.unsplash.com/photo-1585314062340-f1a5a7c9328d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8dGV4dHVyZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60'),
                  fit: BoxFit.cover)),
          child: SafeArea(
            child: Center(
              child: Padding(
                padding: EdgeInsets.all(50),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      //Welcome message
                      Text(
                        'Login',
                        style: TextStyle(
                            fontSize: 35,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 30),
                      //Textfield for Email
                      Container(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 15),
                          child: TextField(
                            controller: _emailController,
                            decoration: InputDecoration(
                              hintText: 'Please enter email',
                              icon: Icon(Icons.email),
                              label: Text('Email'),
                            ),
                            style: GoogleFonts.robotoCondensed(),
                          ),
                        ),
                        decoration: BoxDecoration(
                          border: Border.all(),
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                      SizedBox(height: 15),

                      //Textfield for password
                      Container(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 15),
                          child: TextField(
                            controller: _passwordController,
                            decoration: InputDecoration(
                              hintText: 'Please enter password',
                              icon: Icon(Icons.password_outlined),
                              label: Text('Password'),
                            ),
                            style: GoogleFonts.robotoCondensed(),
                            obscureText: true,
                          ),
                        ),
                        decoration: BoxDecoration(
                          border: Border.all(),
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),

                      //Login button
                      MaterialButton(
                        onPressed: (logIn),
                        color: Colors.green.shade200,
                        child:
                            Text('Login', style: GoogleFonts.robotoCondensed()),
                        //color: Colors.green.shade200,
                      ),
                      SizedBox(height: 15),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('New user?',
                              style: GoogleFonts.robotoCondensed(
                                  color: Colors.white)),
                          MaterialButton(
                            onPressed: widget.SignUpNow,
                            child: Text(
                              'Register here!',
                              style: TextStyle(color: Colors.blue.shade200),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
