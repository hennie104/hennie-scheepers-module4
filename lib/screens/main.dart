import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:trash_away/auth/auth.dart';
import 'package:trash_away/screens/dashboard.dart';
import 'package:trash_away/screens/firebase_options.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:trash_away/screens/splash_screen.dart';
import 'package:trash_away/utils/utils.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
      name: 'trash_away', options: DefaultFirebaseOptions.currentPlatform);

  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      scaffoldMessengerKey: Utils.messengerKey,
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      theme: ThemeData(
          colorScheme: ColorScheme(
              brightness: Brightness.dark,
              primary: Colors.black,
              onPrimary: Colors.white30,
              secondary: Colors.green.shade200,
              onSecondary: Colors.black,
              error: Colors.red.shade200,
              onError: Colors.white,
              background: Colors.transparent,
              onBackground: Colors.black,
              surface: Colors.transparent,
              onSurface: Colors.white),
          inputDecorationTheme: InputDecorationTheme(
            hintStyle: TextStyle(color: Colors.white),
            labelStyle: TextStyle(
              color: Colors.white,
            ),
          ),
          fontFamily: GoogleFonts.robotoCondensed().fontFamily),
    );
  }
}

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: StreamBuilder<User?>(
            stream: FirebaseAuth.instance.authStateChanges(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return const DashBoard();
              } else {
                return AuthPage();
              }
            }),
      );
}
